from bs4 import BeautifulSoup
import argparse
import requests
import string
import pprint


def main(args):
    url_text = get_url_text(args.url)
    counted_text = count_text(url_text)
    pprint.pprint(cutoff_text(counted_text), width=1)


def get_url_text(url):
    page = requests.get(args.url)
    soup = BeautifulSoup(page.text, 'html.parser')
    return soup.body.get_text()


def count_text(text):
    # Returns of dict of each time each word the the page occurs
    text = text.split(' ')
    text_counted = {}
    remove_chars = string.punctuation + string.digits
    for word in text:
        word = word.strip().lower().translate(str.maketrans('', '', remove_chars))
        if word in text_counted.keys():
            text_counted[word] += 1
        else:
            text_counted[word] = 0
    return text_counted

def cutoff_text(text_dict):
    exclude = [
        ''
        ,'a'
        ,'an'
        ,'as'
        ,'and'
        ,'be'
        ,'but'
        ,'by'
        ,'because'
        ,'can'
        ,'can\'t'
        ,'do'
        ,'don\'t'
        ,'done'
        ,'did'
        ,'does'
        ,'from'
        ,'for'
        ,'go'
        ,'help'
        ,'he'
        ,'his'
        ,'him'
        ,'I'
        ,'in'
        ,'it'
        ,'is'
        ,'its'
        ,'me'
        ,'more'
        ,'most'
        ,'no'
        ,'not'
        ,'of'
        ,'off'
        ,'on'
        ,'or'
        ,'so'
        ,'said'
        ,'that'
        ,'this'
        ,'to'
        ,'the'
        ,'they'
        ,'them'
        ,'then'
        ,'their'
        ,'tho'
        ,'us'
        ,'with'
        ,'which'
        ,'was'
        ,'were'
       ]
    return { key:val for key, val in text_dict.items() if val >= int(args.cutoff) and key not in exclude }

if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="URL to scrape and count")
    parser.add_argument("cutoff", help="Only return terms with an equal or greater count than the cutoff")
    args = parser.parse_args()
    main(args)
